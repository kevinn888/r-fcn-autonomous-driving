# Penerapan Metode _Regional Fully Convolutional Network_ _untuk Deteksi Objek di Jalan Raya

Tugas Akhir 1116004 - Kevin Nathaniel
Institut Teknologi Harapan Bangsa

## Dataset

Dataset diambil dari [kaggle](https://www.kaggle.com/solesensei/solesensei_bdd100k) atau [BDD Berkeley](https://bdd-data.berkeley.edu)

## Prerequisites

- Python 3.7.7 (anaconda3)
- Tensorflow-GPU 1.15.0
- Keras 2.2.4
- numpy
- matploblib
- vscode (optional)

## Installation

1.  python versi <= 3.7.7
2.  tensorflow dan keras bisa yang terbaru kecuali diatas atau sama dengan tf ver 2.0.0
3.  dataset dari BDD100k

## Run program (training)

1.  run TrainingGUI.py or training.py (note: if not work change directory inside code manually)
2.  if you choose to use training use 'training.py" --images="location/dataset/folder image" --label="location/dataset/label.json"' to run it from cmd. 
3.  if you want to change parameter you can change it from training.py


## Run program (inference)

1.  run InferenceGUI.py for multi class BDD dataset or run InferenceGUIsc.py for single class BDD dataset (class car).

## Problem

\_get_distribution_energy blablalba
ini karna pake keras sama tensorflow callback solusi ada 3 :

- ganti keras jadi tf.keras
- update tensorflow-gpu kalo udh ada update
- atau buka callback terbawah dr errornya ganti di metode set_model
  jadi:  
   '# In case this callback is used via native Keras, \_get_distribution_strategy does not exist.
  if hasattr(self.model, '\_get_distribution_strategy'): # TensorBoard callback involves writing a summary file in a # possibly distributed settings.
  self.\_log_write_dir = distributed_file_utils.write_dirpath(
  self.log_dir, self.model.\_get_distribution_strategy()) # pylint: disable=protected-access
  else:
  self.\_log_write_dir = self.log_dir'

di bagian self.log_dir, sama di metode on_train_end jadi:

'# In case this callback is used via native Keras, \_get_distribution_strategy does not exist.
if hasattr(self.model, '\_get_distribution_strategy'): # Safely remove the unneeded temp files.
distributed_file_utils.remove_temp_dirpath(
self.log_dir, self.model.\_get_distribution_strategy()) # pylint: disable=protected-access'

## Thanks to
 Terima kasih terhadap matterport, tensorflow team, keras team, dan beberapa user dari stackoverflow yang telah membantu baik berupa ilmu maupun nasihat sehingga project ini dapat terselesaikan dengan baik.

