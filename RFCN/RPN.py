# import keras
# import os
# import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
# import imageio
# import scipy
# import time
# import tensorflow as tf
# from keras.optimizers import Adadelta
# from keras.models import Sequential, load_model, Model
# from keras.layers import Dense, Conv2D, Flatten, MaxPooling2D, Dropout, Input, Add, BatchNormalization, Activation
# from keras.initializers import glorot_uniform
# from keras.activations import relu, softmax
# from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
# from keras.utils import plot_model
# from keras.utils.vis_utils import model_to_dot
# from keras import backend as K
# from sklearn.metrics import confusion_matrix
# from matplotlib import animation, rc

# feature_map_tile = Input(shape=(None, None, 1536))
# convolution_3x3 = Conv2D(
#     filters=512,
#     kernel_size=(3, 3),
#     name="3x3"
# )(feature_map_tile)

# output_deltas = Conv2D(
#     filters=4 * k,
#     kernel_size=(1, 1),
#     activation="linear",
#     kernel_initializer="uniform",
#     name="deltas1"
# )(convolution_3x3)

# output_scores = Conv2D(
#     filters=1 * k,
#     kernel_size=(1, 1),
#     activation="sigmoid",
#     kernel_initializer="uniform",
#     name="scores1"
# )(convolution_3x3)

# model = Model(inputs=[feature_map_tile], outputs=[
#               output_scores, output_deltas])

import keras.models as KM
import keras.layers as KL
import keras.engine as KE
import numpy as np
import tensorflow as tf
from Utils.Utils import batchSlice, clipBoxesGraph


class RPN:
    def __init__(self, anchor_stride, anchors_per_location, depth):
        self.model = self.build_rpn_model(
            anchor_stride, anchors_per_location, depth)

    def build_rpn_model(self, anchor_stride, anchors_per_location, depth):
        input_feature_map = KL.Input(shape=[None, None, depth],
                                     name="input_rpn_feature_map")
        outputs = self.rpn(input_feature_map,
                           anchors_per_location, anchor_stride)
        return KM.Model([input_feature_map], outputs, name="rpn_model")

    def rpn(self, feature_map, anchors_per_location, anchor_stride):

        shared = KL.Conv2D(512, (3, 3), padding='same', activation='relu',
                           strides=anchor_stride,
                           name='rpn_conv_shared')(feature_map)
        # print("mengecek nilai yang ada {}".format(shared.shape))
        # print('separasi')
        # Anchor Score. [batch, height, width, anchors per location * 2].
        x = KL.Conv2D(2 * anchors_per_location, (1, 1), padding='valid',
                      activation='linear', name='rpn_class_raw')(shared)

        # Reshape to [batch, anchors, 2]
        rpn_class_logits = KL.Lambda(
            lambda t: tf.reshape(t, [tf.shape(t)[0], -1, 2]))(x)

        # Softmax on last dimension of BG/FG.
        rpn_probs = KL.Activation(
            "softmax", name="rpn_class_xxx")(rpn_class_logits)

        # Bounding box refinement. [batch, H, W, anchors per location, depth]
        # where depth is [x, y, log(w), log(h)]
        x = KL.Conv2D(anchors_per_location * 4, (1, 1), padding="valid",
                      activation='linear', name='rpn_bbox_pred')(shared)

        # Reshape to [batch, anchors, 4]
        rpn_bbox = KL.Lambda(lambda t: tf.reshape(
            t, [tf.shape(t)[0], -1, 4]))(x)

        return rpn_class_logits, rpn_probs, rpn_bbox


############################################################
#  Proposal Layer
############################################################


def apply_box_deltas_graph(boxes, deltas):
    # Convert to y, x, h, w
    height = boxes[:, 2] - boxes[:, 0]
    width = boxes[:, 3] - boxes[:, 1]
    center_y = boxes[:, 0] + 0.5 * height
    center_x = boxes[:, 1] + 0.5 * width
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= tf.exp(deltas[:, 2])
    width *= tf.exp(deltas[:, 3])
    # Convert back to y1, x1, y2, x2
    y1 = center_y - 0.5 * height
    x1 = center_x - 0.5 * width
    y2 = y1 + height
    x2 = x1 + width
    result = tf.stack([y1, x1, y2, x2], axis=1, name="apply_box_deltas_out")
    return result


def clip_boxes_graph(boxes, window):
    # Split corners
    wy1, wx1, wy2, wx2 = tf.split(window, 4)
    y1, x1, y2, x2 = tf.split(boxes, 4, axis=1)
    # Clip
    y1 = tf.maximum(tf.minimum(y1, wy2), wy1)
    x1 = tf.maximum(tf.minimum(x1, wx2), wx1)
    y2 = tf.maximum(tf.minimum(y2, wy2), wy1)
    x2 = tf.maximum(tf.minimum(x2, wx2), wx1)
    clipped = tf.concat([y1, x1, y2, x2], axis=1, name="clipped_boxes")
    return clipped

#################################################################
#                           PROPOSAL LAYER                      #
#################################################################


class ProposalLayer(KE.Layer):

    def __init__(self, proposal_count, nms_threshold, anchors,
                 config=None, **kwargs):
        super(ProposalLayer, self).__init__(**kwargs)
        self.config = config
        self.proposal_count = proposal_count
        self.nms_threshold = nms_threshold
        self.anchors = anchors.astype(np.float32)

    def call(self, inputs):
        scores = inputs[0][:, :, 1]
        deltas = inputs[1]
        deltas = deltas * np.reshape(self.config.RPN_BBOX_STD_DEV, [1, 1, 4])
        anchors = self.anchors

        pre_nms_limit = min(6000, self.anchors.shape[0])
        ix = tf.nn.top_k(scores, pre_nms_limit, sorted=True,
                         name="top_anchors").indices
        scores = batchSlice([scores, ix], lambda x, y: tf.gather(x, y),
                            self.config.IMAGES_PER_GPU)
        deltas = batchSlice([deltas, ix], lambda x, y: tf.gather(x, y),
                            self.config.IMAGES_PER_GPU)
        anchors = batchSlice(ix, lambda x: tf.gather(anchors, x),
                             self.config.IMAGES_PER_GPU,
                             names=["pre_nms_anchors"])

        # Apply deltas to anchors to get refined anchors.
        # [batch, N, (y1, x1, y2, x2)]
        boxes = batchSlice([anchors, deltas],
                           lambda x, y: apply_box_deltas_graph(
            x, y),
            self.config.IMAGES_PER_GPU,
            names=["refined_anchors"])

        # Clip to image boundaries. [batch, N, (y1, x1, y2, x2)]
        height, width = self.config.IMAGE_SHAPE[:2]
        window = np.array([0, 0, height, width]).astype(np.float32)
        boxes = batchSlice(boxes,
                           lambda x: clip_boxes_graph(
                               x, window),
                           self.config.IMAGES_PER_GPU,
                           names=["refined_anchors_clipped"])

        # Normalize dimensions to range of 0 to 1.
        normalized_boxes = boxes / np.array([[height, width, height, width]])

        # Non-max suppression
        def nms(normalized_boxes, scores):
            indices = tf.image.non_max_suppression(
                normalized_boxes, scores, self.proposal_count,
                self.nms_threshold, name="rpn_non_max_suppression")
            proposals = tf.gather(normalized_boxes, indices)
            # Pad if needed
            padding = tf.maximum(self.proposal_count -
                                 tf.shape(proposals)[0], 0)
            proposals = tf.pad(proposals, [(0, padding), (0, 0)])
            return proposals
        proposals = batchSlice([normalized_boxes, scores], nms,
                               self.config.IMAGES_PER_GPU)
        return proposals

    def compute_output_shape(self, input_shape):
        return (None, self.proposal_count, 4)

############################################################
#  Detection Target Layer
############################################################


def overlaps_graph(boxes1, boxes2):
    b1 = tf.reshape(tf.tile(tf.expand_dims(boxes1, 1),
                            [1, 1, tf.shape(boxes2)[0]]), [-1, 4])
    b2 = tf.tile(boxes2, [tf.shape(boxes1)[0], 1])
    # 2. Compute intersections
    b1_y1, b1_x1, b1_y2, b1_x2 = tf.split(b1, 4, axis=1)
    b2_y1, b2_x1, b2_y2, b2_x2 = tf.split(b2, 4, axis=1)
    y1 = tf.maximum(b1_y1, b2_y1)
    x1 = tf.maximum(b1_x1, b2_x1)
    y2 = tf.minimum(b1_y2, b2_y2)
    x2 = tf.minimum(b1_x2, b2_x2)
    intersection = tf.maximum(x2 - x1, 0) * tf.maximum(y2 - y1, 0)
    # 3. Compute unions
    b1_area = (b1_y2 - b1_y1) * (b1_x2 - b1_x1)
    b2_area = (b2_y2 - b2_y1) * (b2_x2 - b2_x1)
    union = b1_area + b2_area - intersection
    # 4. Compute IoU and reshape to [boxes1, boxes2]
    iou = intersection / union
    overlaps = tf.reshape(iou, [tf.shape(boxes1)[0], tf.shape(boxes2)[0]])
    return overlaps
