import os
import logging
import tkinter as tk
from tkinter import filedialog, Canvas, NW, PhotoImage
from Inference import Test, RFCNNConfig
from RFCN.Model import Model
from PIL import ImageTk, Image


def askDirectory():
    global file_path
    global b
    directory = filedialog.askdirectory()
    file_path.set(directory)
    b['state'] = "normal"


def askFile(canvas):
    global file_path
    global b
    directory = filedialog.askopenfilename(title="Select file", filetypes=(
        ("JPEG images", ".jpg"), ("all files", "*.*")))
    file_path.set(directory)
    b['state'] = "normal"
    # show_original_image(file_path.get())
    make_image(file_path.get(), canvas)


# def runCommand(text):
#     print(text)
#     label_path = os.path.join(text.get(), 'label')
#     images_path = os.path.join(text.get(), 'images/100k')
#     Path_args = os.path.join(
#         os.getcwd(), 'inference.py" --images="{}" --label="{}"'.format(images_path, label_path))
#     os.path.normpath
#     cmd = 'CMD /C C:/Users/kevin/anaconda3/envs/tf-gpu/python.exe "' + Path_args
#     print(cmd)
#     os.system(cmd)


def run_inference(path):
    print(path)
    ROOT_DIR = os.getcwd()
    config = RFCNNConfig()

    model = Model(mode="inference", config=config,
                       modelDir=os.path.join(ROOT_DIR, "model-data"))

    time = Test(model, path.get(), os.path.join(ROOT_DIR, "result"))
    # show_result(time)


def readImage(path):
    ROOT_DIR = os.getcwd()
    # global canvas
    if path:
        return ImageTk.PhotoImage(Image.open(path))
        # canvas.create_image(20, 20, anchor=NW, image=img)
    else:
        return ImageTk.PhotoImage(Image.open(os.path.join(ROOT_DIR, 'default.jpg')))
        # canvas.create_image(20, 20, anchor=NW, image=img)


# def changepic(imagename):
#     print(imagename)
#     img2 = ImageTk.PhotoImage(Image.open(imagename))
#     canvas.itemconfig(imageArea, image=img2)
#     canvas.update()

def show_result(time):
    window = tk.Toplevel(m)
    window.title('Info')
    label_info = tk.Label(
        window, text='image has been processed, process took {} sec'.format(time))
    label_info.pack()
    # canvas = Canvas(window, width=1280, height=720)
    # canvas.grid(column=1)
    # img = ImageTk.PhotoImage(Image.open(filename))
    # canvas.create_image(20, 20, anchor=NW, image=img)


def make_image(filename, canvas):
    photo = ImageTk.PhotoImage(Image.open(filename))
    myCanvas.image = photo
    myCanvas.create_image(0, 0, image=photo, anchor="nw")
    myCanvas.update()


if __name__ == "__main__":
    print(os.getcwd())

    m = tk.Tk()
    m.title("RFCN Inference")
    # m.withdraw()
    file_path = tk.StringVar()
    button_frame = tk.Frame(m, height=100, width=1280)
    label_frame = tk.Frame(m, height=100, width=1280)
    b = tk.Button(button_frame, text='Testing', width=25,
                  command=lambda x=file_path: run_inference(x), bg="#FF5733")
    b1 = tk.Button(button_frame, text='Exit', width=25,
                   command=m.destroy)
    b['state'] = 'disabled'
    # openFile = tk.Button(m, text='open Images Directory',
    #                      width=25, command=askDirectory)

    myCanvas = Canvas(m, width=1280, height=720)
    openFile = tk.Button(button_frame, text='open Image File',
                         width=25, command=lambda x=myCanvas: askFile(x))
    myCanvas.grid(row=0, column=1)
    make_image("default.jpg", myCanvas)

    label_frame.grid(row=1, column=1)
    w1 = tk.Label(label_frame, text="dataset directory :")
    w1.grid(row=0, column=0)
    w2 = tk.Label(label_frame, textvariable=file_path)
    w2.grid(row=0, column=1)
    button_frame.grid(row=2, column=1, pady=(0, 10))
    button_frame.columnconfigure(0, weight=1)
    button_frame.columnconfigure(1, weight=1)
    button_frame.columnconfigure(2, weight=1)
    button_frame.columnconfigure(3, weight=1)
    openFile.grid(row=0, column=0, padx=10)
    w3 = tk.Label(
        button_frame, text="Model will be save in directory '../Model-data'")
    w3.grid(row=0, column=1, padx=(50, 30))
    b.grid(row=0, column=2)
    b1.grid(row=0, column=3, padx=10)
    m.mainloop()
