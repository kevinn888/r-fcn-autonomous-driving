import os
from Utils.Dataset import DeepDriveDataset
from Utils.Config import Config
from RFCN.Model import Model, MeanAveragePrecisionCallback
import os
from keras.models import load_model
import numpy as np
import pickle
import argparse
from PIL import Image
import json
import tensorflow
import keras


class RFCNNConfig(Config):
    """Configuration for training on the toy shapes dataset.
    Derives from the base Config class and overrides values specific
    to the toy shapes dataset.
    """
    # Give the configuration a recognizable name
    NAME = "BDD"

    # Backbone model
    BACKBONE = "resnet50"

    # Train on 1 GPU and 8 images per GPU. We can put multiple images on each
    # GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

    # Number of classes (including background)
    C = 1 + 11  # background + 2 tags
    NUM_CLASSES = C
    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 800
    IMAGE_MAX_DIM = 1280

    # Use smaller anchors because our image and objects are small
    RPN_ANCHOR_SCALES = (32, 64, 128, 256, 512)  # anchor side in pixels
    # Use same strides on stage 4-6 if use dilated resnet of DetNet
    # Like BACKBONE_STRIDES = [4, 8, 16, 16, 16]
    BACKBONE_STRIDES = [4, 8, 16, 32, 64]
    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    TRAIN_ROIS_PER_IMAGE = 200

    # Use a small epoch since the data is simple
    STEPS_PER_EPOCH = 1000

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 200

    RPN_NMS_THRESHOLD = 0.7
    POOL_SIZE = 7


def main():

    # check status from tensorflow
    # tensorflow.compat.v1.disable_resource_variables()
    # with tensorflow.compat.v1.Session() as ses:
    # ses = tensorflow.compat.v1.Session()
    # print(tensorflow.executing_eagerly())
    # tensorflow.debugging.set_log_device_placement(True)

    ROOT_DIR = os.getcwd()
    parser = argparse.ArgumentParser()
    parser.add_argument('--images', required=False,
                        default="images/",
                        metavar="training images loadpath",
                        help="training images loadpath")
    parser.add_argument('--label', required=False,
                        default="result/",
                        metavar="label images loadpath",
                        help="label images loadpath")

    # # inisialisasi config
    config = RFCNNConfig()
    args = parser.parse_args()
    images_train = os.path.join(args.images, "train")
    label_train = os.path.join(args.label, "detection_image_train.json")
    images_val = os.path.join(args.images, "val")
    label_val = os.path.join(args.label, "detection_image_val.json")

    print('================================= LOAD DATASET ====================================')
    print("dataset image train directory : ", images_train)
    print("dataset label train directory : ", label_train)
    print("dataset image val directory : ", images_val)
    print("dataset label val directory : ", label_val)
    print('===================================================================================')
    # inisialisasi dataset training
    dataset_train = DeepDriveDataset()
    dataset_train.initDB(label_train, images_train, 70000)
    dataset_train.prepare()

    # Validation dataset
    dataset_val = DeepDriveDataset()
    dataset_val.initDB(label_val, images_val, 10000)
    dataset_val.prepare()
    # siapin model rfcn untuk training
    model = Model(mode="training", config=config,
                       modelDir=os.path.join(ROOT_DIR, os.path.join(ROOT_DIR, "model-data")), env="debug")
    #
    model_inference = Model(mode="inference", config=config,
                                 modelDir=os.path.join(ROOT_DIR, "model-data"), env="debug")

    # # # init = tf.global_variables_initializer()
    # # model.load_weights(
    # #     'D:/Workspace/College/Semester 8/Tugas Akhir/Keras-RFCN-master/Keras-RFCN-master/ModelData/bdd20200430T2145/Keras-RFCN_bdd_0007.h5', by_name=True)
    # # model.load_optimizer(
    # # #     "D:/Workspace/College/Semester 8/Tugas Akhir/Keras-RFCN-master/Keras-RFCN-master/ModelData/optimizer_0007.pkl")
    # model = load_model("D:/Workspace/College/Semester 8/Tugas Akhir/Keras-RFCN-master/Keras-RFCN-master/ModelData/bdd20200502T2342/Keras-RFCN_bdd_epoch.h5", custom_objects={
    #                    'BatchNorm': BatchNorm, 'tf': tensorflow, 'ProposalLayer': ProposalLayer, 'DetectionTargetLayer': DetectionTargetLayer, 'VotePooling': VotePooling, 'gtBoxes': gtBoxes, 'parse_image_meta_graph': parse_image_meta_graph, 'KerasRFCN': KerasRFCN}, compile=False)
    # model.load_weights(
    #     'D:/Workspace/College/Semester 8/Tugas Akhir/Keras-RFCN-master/Keras-RFCN-master/ModelData/test.h5')
    # with open('D:/Workspace/College/Semester 8/Tugas Akhir/Keras-RFCN-master/Keras-RFCN-master/ModelData/optimizer_0000.pkl', 'rb') as f:
    #     weight_values = pickle.load(f)
    #     model.optimizer.set_weights(weight_values)

    try:
        model_path = model.find_last()[1]
        if model_path is not None:
            model.load_weights(model_path, by_name=True)
    except Exception as e:
        print(e)
        print("No checkpoint founded")

    # # resume training begin
    # # stage 1
    # model.resume_train(dataset_train, dataset_val,
    #                    learning_rate=config.LEARNING_RATE,
    #                    epochs=10,
    #                    layers='heads')

    # custom callback
    mean_average_precision_callback = MeanAveragePrecisionCallback(
        model, model_inference, dataset_val, everyEpoch=2, verbose=1)
    # # # # tensorflow.compat.v1.enable_resource_variables()
    # # # training begin
    # stage 1

    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=10,
                layers='heads',
                customCallback=mean_average_precision_callback)

    # Training - Stage 2
    # Finetune layers from ResNet stage 4 and up
    print("Fine tune Resnet stage 4 and up")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=40,
                layers='4+',
                customCallback=mean_average_precision_callback)

    # Training - Stage 3
    # Fine tune all layers
    print("Fine tune all layers")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=80,
                layers='all',
                customCallback=mean_average_precision_callback)

    # Training - Stage 3
    # Fine tune all layers
    print("Fine tune all layers")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=240,
                layers='all',
                customCallback=mean_average_precision_callback)


if __name__ == "__main__":
    # with tensorflow.compat.v1.Session() as ses:
    #     ses.run(tensorflow.global_variables_initializer())
    #     sess.run(main())
    main()
