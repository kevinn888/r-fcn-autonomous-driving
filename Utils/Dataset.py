# pylint: disable=unsubscriptable-object
# pylint: disable=relative-beyond-top-level
import os
# from KerasRFCN.Model.Model import RFCN_Model
# from KerasRFCN.Config import Config

# from KerasRFCN.Utils import Dataset, parseLabel, parseLabelDetection
from .Utils import parseLabelDetection, generatePyramidAnchors, resize_image, resize_bbox, compose_image_meta, compute_overlaps
from PIL import Image
import numpy as np
import pickle
import json
import skimage.color
import skimage.io
import logging
############################################################
#  Dataset
############################################################


class Dataset(object):

    def __init__(self, verbose=0):
        self._image_ids = []
        self.image_info = []
        self.verbose = verbose
        # Background is always the first class
        self.class_info = [{"source": "", "id": 0, "name": "BG"}]
        self.source_class_ids = {}

    def add_class(self, source, class_id, class_name):
        assert "." not in source, "Source name cannot contain a dot"

        for info in self.class_info:
            if info['source'] == source and info["id"] == class_id:
                return
        # Add the class
        self.class_info.append({
            "source": source,
            "id": class_id,
            "name": class_name,
        })

    def add_image(self, source, image_id, **kwargs):
        image_info = {
            "id": image_id,
            "source": source
        }
        image_info.update(kwargs)
        self.image_info.append(image_info)

    def prepare(self):
        """Prepares the Dataset class for use."""
        def clean_name(name):
            """Returns object names (debugging check/cleaner)."""
            return ",".join(name.split(",")[:1])

        # Build (or rebuild) everything else from the info dicts.
        self.num_classes = len(self.class_info)
        self.class_ids = np.arange(self.num_classes)
        self.class_names = [clean_name(c["name"]) for c in self.class_info]
        self.num_images = len(self.image_info)
        self._image_ids = np.arange(self.num_images)

        self.class_from_source_map = {"{}.{}".format(info['source'], info['id']): id
                                      for info, id in zip(self.class_info, self.class_ids)}

        # Map sources to class_ids they support
        self.sources = list(set([i['source'] for i in self.class_info]))
        self.source_class_ids = {}
        # Loop over datasets
        for source in self.sources:
            self.source_class_ids[source] = []
            # Find classes that belong to this dataset
            for i, info in enumerate(self.class_info):
                # Include BG class in all datasets
                if i == 0 or source == info['source']:
                    self.source_class_ids[source].append(i)

    def append_data(self, class_info, image_info):
        self.external_to_class_id = {}
        for i, c in enumerate(self.class_info):
            for ds, id in c["map"]:
                self.external_to_class_id[ds + str(id)] = i

        # Map external image IDs to internal ones.
        self.external_to_image_id = {}
        for i, info in enumerate(self.image_info):
            self.external_to_image_id[info["ds"] + str(info["id"])] = i

    @property
    def image_ids(self):
        return self._image_ids

    def load_image(self, image_id):
        """
        Load image
        """
        # Load image
        image = skimage.io.imread(self.image_info[image_id]['path'])
        # If grayscale. Convert to RGB for consistency.
        if image.ndim != 3:
            image = skimage.color.gray2rgb(image)
        return image

    def load_bbox(self, image_id):
        # Override this function to load a bbox from your dataset.
        # Otherwise, it returns an empty bbox.
        bbox = np.empty([0, 0, 0])
        class_ids = np.empty([0], np.int32)
        return bbox, class_ids


class DeepDriveDataset(Dataset):
    # count - int, images in the dataset
    def initDB(self, annotation, bddDir, count, start=0):
        self.start = start

        # image_dir = "/content/bdd100k/"
        # classesCount debugging purpose only
        all_images, classesCount, classesMapping = parseLabelDetection(
            annotation)

        if self.verbose == 1:
            print("number of image for each classes :")
            print(classesCount)
        self.classes = {}
        # Add classes (k class, c index) g bs pake class karna reference
        for k, c in classesMapping.items():
            self.add_class("Driving", c, k)
            self.classes[c] = k

        # print('add image')
        for k, item in enumerate(all_images[start:count+start]):
            self.add_image(source="Driving", image_id=k,
                           filename=item['name'], width=1280, height=720, bboxes=item['bbox'])
        # print("finish add image")
        self.rootpath = bddDir

    # read image from file and get the
    def load_image(self, image_id):
        info = self.image_info[image_id]
        # tempImg = image.img_to_array( image.load_img(info['path']) )
        tempImg = np.array(Image.open(
            os.path.join(self.rootpath, info['filename'])))
        return tempImg

    def get_keys(self, d, value):
        return [k for k, v in d.items() if v == value]

    def load_bbox(self, image_id):
        info = self.image_info[image_id]
        bboxes = []
        labels = []

        for item in info['bboxes']:
            bboxes.append((item['y1'], item['x1'], item['y2'], item['x2']))
            label_key = self.get_keys(self.classes, item['category'])
            if len(label_key) == 0:
                continue
            labels.extend(label_key)
        return np.array(bboxes), np.array(labels)


########################################################
#                  Data Genenerator                    #
########################################################


def dataGenerator(dataset, shuffle=True, augment=True, random_rois=0,
                  batch_size=1, detection_targets=False):
    """
    A generator that returns images and corresponding target class ids,
    bounding box deltas.
    """
    b = 0  # batch item index
    image_index = -1
    image_ids = np.copy(dataset.image_ids)
    error_count = 0

    print('data generator')
    # Anchors
    # [anchor_count, (y1, x1, y2, x2)]
    anchors = generatePyramidAnchors((32, 64, 128, 256, 512),
                                     [0.5, 1, 2],
                                     np.array([[320, 320],
                                               [160, 160],
                                               [80,  80],
                                               [40,  40],
                                               [20,  20]]),
                                     [4, 8, 16, 32, 64],
                                     1)

    # Keras requires a generator to run indefinately.
    while True:
        # Increment index to pick next image. Shuffle if at the start of an epoch.
        print(len(image_ids))
        image_index = (image_index + 1) % len(image_ids)
        if shuffle and image_index == 0:
            np.random.shuffle(image_ids)

        # Get GT bounding boxes for image.
        image_id = image_ids[image_index]
        try:
            image, image_meta, gt_class_ids, gt_boxes = \
                load_image_gt(dataset, image_id, augment=augment)

            if not np.any(gt_class_ids > 0):
                continue

            # RPN Targets
            rpn_match, rpn_bbox = build_rpn_targets(image.shape, anchors,
                                                    gt_class_ids, gt_boxes)

            # Init batch arrays
            if b == 0:
                batch_image_meta = np.zeros(
                    (batch_size,) + image_meta.shape, dtype=image_meta.dtype)
                batch_rpn_match = np.zeros(
                    [batch_size, anchors.shape[0], 1], dtype=rpn_match.dtype)
                batch_rpn_bbox = np.zeros(
                    [batch_size, 256, 4], dtype=rpn_bbox.dtype)
                batch_images = np.zeros(
                    (batch_size,) + image.shape, dtype=np.float32)
                batch_gt_class_ids = np.zeros(
                    (batch_size, 100), dtype=np.int32)
                batch_gt_boxes = np.zeros(
                    (batch_size, 100, 4), dtype=np.int32)

            # If more instances than fits in the array, sub-sample from them.
            if gt_boxes.shape[0] > 100:
                ids = np.random.choice(
                    np.arange(gt_boxes.shape[0]), 100, replace=False)
                gt_class_ids = gt_class_ids[ids]
                gt_boxes = gt_boxes[ids]

            # Add to batch
            batch_image_meta[b] = image_meta
            batch_rpn_match[b] = rpn_match[:, np.newaxis]
            batch_rpn_bbox[b] = rpn_bbox
            batch_images[b] = mold_image(image.astype(np.float32))
            batch_gt_class_ids[b, :gt_class_ids.shape[0]] = gt_class_ids
            batch_gt_boxes[b, :gt_boxes.shape[0]] = gt_boxes
            b += 1

            # Batch full?
            if b >= batch_size:
                inputs = [batch_images, batch_image_meta, batch_rpn_match, batch_rpn_bbox,
                          batch_gt_class_ids, batch_gt_boxes]
                outputs = []

                yield inputs, outputs

                # start a new batch
                b = 0
        except (GeneratorExit, KeyboardInterrupt):
            raise
        except:
            # Log it and skip the image
            print(image_id)
            logging.exception("Error processing image {}".format(
                dataset.image_info[image_id]))
            error_count += 1
            if error_count > 5:
                raise


def build_rpn_targets(image_shape, anchors, gt_class_ids, gt_boxes):
    # RPN Match: 1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_match = np.zeros([anchors.shape[0]], dtype=np.int32)
    # RPN bounding boxes: [max anchors per image, (dy, dx, log(dh), log(dw))]
    rpn_bbox = np.zeros((256, 4))

    crowd_ix = np.where(gt_class_ids < 0)[0]
    if crowd_ix.shape[0] > 0:
        non_crowd_ix = np.where(gt_class_ids > 0)[0]
        crowd_boxes = gt_boxes[crowd_ix]
        gt_class_ids = gt_class_ids[non_crowd_ix]
        gt_boxes = gt_boxes[non_crowd_ix]
        # Compute overlaps with crowd boxes [anchors, crowds]
        crowd_overlaps = compute_overlaps(anchors, crowd_boxes)
        crowd_iou_max = np.amax(crowd_overlaps, axis=1)
        no_crowd_bool = (crowd_iou_max < 0.001)
    else:
        # All anchors don't intersect a crowd
        no_crowd_bool = np.ones([anchors.shape[0]], dtype=bool)

    overlaps = compute_overlaps(anchors, gt_boxes)

    anchor_iou_argmax = np.argmax(overlaps, axis=1)
    anchor_iou_max = overlaps[np.arange(overlaps.shape[0]), anchor_iou_argmax]
    rpn_match[(anchor_iou_max < 0.3) & (no_crowd_bool)] = -1

    gt_iou_argmax = np.argmax(overlaps, axis=0)
    rpn_match[gt_iou_argmax] = 1
    rpn_match[anchor_iou_max >= 0.7] = 1

    ids = np.where(rpn_match == 1)[0]
    extra = len(ids) - (200 // 2)
    if extra > 0:
        # Reset the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0
    # Same for negative proposals
    ids = np.where(rpn_match == -1)[0]
    extra = len(ids) - (200 -
                        np.sum(rpn_match == 1))
    if extra > 0:
        # Rest the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0

    # For positive anchors, compute shift and scale needed to transform them
    # to match the corresponding GT boxes.
    ids = np.where(rpn_match == 1)[0]
    ix = 0  # index into rpn_bbox
    for i, a in zip(ids, anchors[ids]):
        # Closest gt box (it might have IoU < 0.7)
        gt = gt_boxes[anchor_iou_argmax[i]]

        # Convert coordinates to center plus width/height.
        # GT Box
        gt_h = gt[2] - gt[0]
        gt_w = gt[3] - gt[1]
        gt_center_y = gt[0] + 0.5 * gt_h
        gt_center_x = gt[1] + 0.5 * gt_w
        # Anchor
        a_h = a[2] - a[0]
        a_w = a[3] - a[1]
        a_center_y = a[0] + 0.5 * a_h
        a_center_x = a[1] + 0.5 * a_w

        # Compute the bbox refinement that the RPN should predict.
        rpn_bbox[ix] = [
            (gt_center_y - a_center_y) / a_h,
            (gt_center_x - a_center_x) / a_w,
            np.log(gt_h / a_h),
            np.log(gt_w / a_w),
        ]
        # Normalize
        rpn_bbox[ix] /= np.array([0.1, 0.1, 0.2, 0.2])
        ix += 1

    return rpn_match, rpn_bbox


def load_image_gt(dataset, image_id, augment=False):
    # Load image and mask
    image = dataset.load_image(image_id)
    # bbox: [num_instances, (y1, x1, y2, x2)]
    bboxes, class_ids = dataset.load_bbox(image_id)
    shape = image.shape
    image, window, scale, padding = resize_image(
        image,
        min_dim=800,
        max_dim=1280,
        padding=True)
    bboxes = resize_bbox(bboxes, scale, padding)
    active_class_ids = np.zeros([dataset.num_classes], dtype=np.int32)
    source_class_ids = dataset.source_class_ids[dataset.image_info[image_id]["source"]]
    active_class_ids[source_class_ids] = 1

    # Image meta data
    image_meta = compose_image_meta(image_id, shape, window, active_class_ids)

    return image, image_meta, class_ids, bboxes


def mold_image(images):
    """Takes RGB images with 0-255 values and subtraces
    the mean pixel and converts it to float. Expects image
    colors in RGB order.
    """
    RGB_MEAN = np.array([123.7, 116.8, 103.9])
    return images.astype(np.float32) - RGB_MEAN
