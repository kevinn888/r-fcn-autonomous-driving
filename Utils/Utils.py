# pylint: disable=unsubscriptable-object
import os
import json
import numpy as np
import keras.layers as KL
import keras.backend as K
import tensorflow as tf
from PIL import Image
#################################################################
#                       DATASET FORMATTING                      #
#################################################################


def parseLabel(bddDir):
    allImg = {}
    classesCount = {}
    with open(bddDir, 'r') as f:
        bddLabel = json.load(f)

    for img in bddLabel:
        labels = img['labels']
        allImg[img['name']] = {}
        allImg[img['name']]['bboxes'] = []
        for label in labels:
            if 'box2d' in label:
                if label['category'] not in classesCount:
                    classesCount[label['category']] = 1
                else:
                    classesCount[label['category']] += 1

                # jaga-jaga kalo butuh width dan height disini
                # print(label['category'])
                allImg[img['name']]['bboxes'].append({'className': label['category'], 'x1': int(label['box2d']['x1']),
                                                      'x2': int(label['box2d']['x2']), 'y1': int(label['box2d']['y1']),
                                                      'y2': int(label['box2d']['y2'])})
    print(len(allImg))
    print(classesCount)
    return allImg, classesCount


def parseLabelDetection(bddDir):
    # allImg = {}
    classesCount = {}
    with open(bddDir, 'r') as f:
        bddLabel = json.load(f)

    for img in bddLabel:
        for bbox in img['bbox']:
            if bbox['category'] not in classesCount:
                classesCount[bbox['category']] = 1
            else:
                classesCount[bbox['category']] += 1
    # print(len(allImg))
    classesMapping = {"traffic light": 0, 'traffic sign': 1, 'car': 2, 'person': 3,
                      'bus': 4, 'truck': 5, 'rider': 6, 'bike': 7, 'motor': 8, 'train': 9}
    return bddLabel, classesCount, classesMapping


def parseImageMetaGraph(meta):
    """
    Parses a tensor that contains image attributes to its components.
    """
    image_id = meta[:, 0]
    image_shape = meta[:, 1:4]
    window = meta[:, 4:8]
    active_class_ids = meta[:, 8:]
    return [image_id, image_shape, window, active_class_ids]


def resize_image(image, min_dim=None, max_dim=None, padding=False):
    """
    Resizes an image keeping the aspect ratio.
    """
    # Default window (y1, x1, y2, x2) and default scale == 1.
    h, w = image.shape[:2]
    window = (0, 0, h, w)
    scale = 1

    # Scale?
    if min_dim:
        # Scale up but not down
        scale = max(1, min_dim / min(h, w))
    # Does it exceed max dim?
    if max_dim:
        image_max = max(h, w)
        if round(image_max * scale) > max_dim:
            scale = max_dim / image_max
    # Resize image and mask
    if scale != 1:
        image = np.array(Image.fromarray(image).resize(
            size=(round(h * scale), round(w * scale))))
    # Need padding?
    if padding:
        # Get new height and width
        h, w = image.shape[:2]
        top_pad = (max_dim - h) // 2
        bottom_pad = max_dim - h - top_pad
        left_pad = (max_dim - w) // 2
        right_pad = max_dim - w - left_pad
        padding = [(top_pad, bottom_pad), (left_pad, right_pad), (0, 0)]
        image = np.pad(image, padding, mode='constant', constant_values=0)
        window = (top_pad, left_pad, h + top_pad, w + left_pad)
    return image, window, scale, padding


def resize_bbox(boxes, scale, padding):
    top_pad = padding[0][0]
    left_pad = padding[1][0]

    resized_boxes = []
    for box in boxes:
        temp_new_box = box * scale
        y1 = temp_new_box[0] + top_pad
        x1 = temp_new_box[1] + left_pad
        y2 = temp_new_box[2] + top_pad
        x2 = temp_new_box[3] + left_pad
        resized_boxes.append((y1, x1, y2, x2))
    return np.array(resized_boxes)


def compose_image_meta(image_id, image_shape, window, active_class_ids):
    meta = np.array(
        [image_id] +            # size=1
        list(image_shape) +     # size=3
        list(window) +          # size=4 (y1, x1, y2, x2) in image cooredinates
        list(active_class_ids)  # size=num_classes
    )
    return meta

#################################################################
#                           RPN Utils                           #
#################################################################


def generateAnchors(scales, ratios, shape, feature_stride, anchor_stride):
    # Get all combinations of scales and ratios (9 anchors)
    scales, ratios = np.meshgrid(np.array(scales), np.array(ratios))
    scales = scales.flatten()
    ratios = ratios.flatten()

    # Enumerate heights and widths from scales and ratios
    heights = scales / np.sqrt(ratios)
    widths = scales * np.sqrt(ratios)

    # Enumerate shifts in feature space
    shifts_y = np.arange(0, shape[0], anchor_stride) * feature_stride
    shifts_x = np.arange(0, shape[1], anchor_stride) * feature_stride
    shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y)

    # Enumerate combinations of shifts, widths, and heights
    box_widths, box_centers_x = np.meshgrid(widths, shifts_x)
    box_heights, box_centers_y = np.meshgrid(heights, shifts_y)

    # Reshape to get a list of (y, x) and a list of (h, w)
    box_centers = np.stack(
        [box_centers_y, box_centers_x], axis=2).reshape([-1, 2])
    box_sizes = np.stack([box_heights, box_widths], axis=2).reshape([-1, 2])

    # Convert to corner coordinates (y1, x1, y2, x2)
    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
                            box_centers + 0.5 * box_sizes], axis=1)
    return boxes


def generatePyramidAnchors(scales, ratios, feature_shapes, feature_strides, anchor_stride):
    # Anchors
    # [anchor_count, (y1, x1, y2, x2)]
    anchors = []
    for i in range(len(scales)):
        anchors.append(generateAnchors(scales[i], ratios, feature_shapes[i],
                                       feature_strides[i], anchor_stride))
    return np.concatenate(anchors, axis=0)


def batchSlice(inputs, graph_fn, batchSize=1, names=None):
    """
    avoid raw tensor wandering on model
    """
    if not isinstance(inputs, list):
        inputs = [inputs]

    outputs = []
    # not working properly (windows & new model)
    for i in range(batchSize):
        inputs_slice = [x[i] for x in inputs]
        output_slice = graph_fn(*inputs_slice)
        if not isinstance(output_slice, (tuple, list)):
            output_slice = [output_slice]
        outputs.append(output_slice)
    # Change outputs from a list of slices where each is
    # a list of outputs to a list of outputs and each has
    # a list of slices
    outputs = list(zip(*outputs))
    print("outputs : ", outputs)
    if names is None:
        names = [None] * len(outputs)

    result = [tf.stack(o, axis=0, name=n)
              for o, n in zip(outputs, names)]
    if len(result) == 1:
        result = result[0]

    return result


def clipBoxesGraph(boxes, window):
    # Split corners
    wy1, wx1, wy2, wx2 = tf.split(window, 4)
    y1, x1, y2, x2 = tf.split(boxes, 4, axis=1)
    # Clip
    y1 = tf.maximum(tf.minimum(y1, wy2), wy1)
    x1 = tf.maximum(tf.minimum(x1, wx2), wx1)
    y2 = tf.maximum(tf.minimum(y2, wy2), wy1)
    x2 = tf.maximum(tf.minimum(x2, wx2), wx1)
    clipped = tf.concat([y1, x1, y2, x2], axis=1, name="clipped_boxes")
    return clipped


def log2(x):
    """Implementatin of Log2. TF doesn't have a native implemenation."""
    return tf.math.log(x) / tf.math.log(2.0)


def apply_box_deltas_graph(boxes, deltas):
    """
    Applies the given deltas to the given boxes.
    """
    # Convert to y, x, h, w
    height = boxes[:, 2] - boxes[:, 0]
    width = boxes[:, 3] - boxes[:, 1]
    center_y = boxes[:, 0] + 0.5 * height
    center_x = boxes[:, 1] + 0.5 * width
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= tf.exp(deltas[:, 2])
    width *= tf.exp(deltas[:, 3])
    # Convert back to y1, x1, y2, x2
    y1 = center_y - 0.5 * height
    x1 = center_x - 0.5 * width
    y2 = y1 + height
    x2 = x1 + width
    result = tf.stack([y1, x1, y2, x2], axis=1, name="apply_box_deltas_out")
    return result


def box_refinement_graph(box, gt_box):
    """Compute refinement needed to transform box to gt_box.
    box and gt_box are [N, (y1, x1, y2, x2)]
    """
    box = tf.cast(box, "float32")
    gt_box = tf.cast(gt_box, "float32")

    height = box[:, 2] - box[:, 0]
    width = box[:, 3] - box[:, 1]
    center_y = box[:, 0] + 0.5 * height
    center_x = box[:, 1] + 0.5 * width

    gt_height = gt_box[:, 2] - gt_box[:, 0]
    gt_width = gt_box[:, 3] - gt_box[:, 1]
    gt_center_y = gt_box[:, 0] + 0.5 * gt_height
    gt_center_x = gt_box[:, 1] + 0.5 * gt_width

    dy = (gt_center_y - center_y) / height
    dx = (gt_center_x - center_x) / width
    dh = tf.math.log(gt_height / height)
    dw = tf.math.log(gt_width / width)

    result = tf.stack([dy, dx, dh, dw], axis=1)
    return result


def batch_pack_graph(x, counts, num_rows):
    """Picks different number of values from each row
    in x depending on the values in counts.
    """
    outputs = []
    for i in range(num_rows):
        outputs.append(x[i, :counts[i]])
    return tf.concat(outputs, axis=0)


def batch_slice(inputs, graph_fn, batch_size, names=None):
    if not isinstance(inputs, list):
        inputs = [inputs]

    outputs = []
    for i in range(batch_size):
        inputs_slice = [x[i] for x in inputs]
        output_slice = graph_fn(*inputs_slice)
        if not isinstance(output_slice, (tuple, list)):
            output_slice = [output_slice]
        outputs.append(output_slice)
    # Change outputs from a list of slices where each is
    # a list of outputs to a list of outputs and each has
    # a list of slices
    outputs = list(zip(*outputs))
    print("outputs : ", outputs)
    if names is None:
        names = [None] * len(outputs)

    result = [tf.stack(o, axis=0, name=n)
              for o, n in zip(outputs, names)]
    if len(result) == 1:
        result = result[0]

    return result


def trim_zeros(x):
    """It's common to have tensors larger than the available data and
    pad with zeros. This function removes rows that are all zeros.

    x: [rows, columns].
    """
    assert len(x.shape) == 2
    return x[~np.all(x == 0, axis=1)]


# for calculate mean average (acuraccy model in inference mode)
def compute_iou(box, boxes, box_area, boxes_area):
    """
    Calculates IoU of the given box with the array of the given boxes.
    """
    # Calculate intersection areas
    y1 = np.maximum(box[0], boxes[:, 0])
    y2 = np.minimum(box[2], boxes[:, 2])
    x1 = np.maximum(box[1], boxes[:, 1])
    x2 = np.minimum(box[3], boxes[:, 3])
    # print(box_area, boxes_area)
    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)
    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union
    return iou


def compute_ap(gt_boxes, gt_class_ids,
               pred_boxes, pred_class_ids, pred_scores,
               iou_threshold=0.5):
    """Compute Average Precision at a set IoU threshold (default 0.5).

    Returns:
    mAP: Mean Average Precision
    precisions: List of precisions at different class score thresholds.
    recalls: List of recall values at different class score thresholds.
    overlaps: [pred_boxes, gt_boxes] IoU overlaps.
    """
    # Trim zero padding and sort predictions by score from high to low
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    pred_boxes = trim_zeros(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]

    # Compute IoU overlaps [pred_boxes, gt_boxes]
    overlaps = compute_overlaps(pred_boxes, gt_boxes)

    # Loop through ground truth boxes and find matching predictions
    match_count = 0
    pred_match = np.zeros([pred_boxes.shape[0]])
    gt_match = np.zeros([gt_boxes.shape[0]])
    # print("overlaps :", overlaps)
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] == 1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            if iou < iou_threshold:
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                match_count += 1
                gt_match[j] = 1
                pred_match[i] = 1
                break

    # Compute precision and recall at each prediction box step
    precisions = np.cumsum(pred_match) / (np.arange(len(pred_match)) + 1)
    print('precision', precisions)
    recalls = np.cumsum(pred_match).astype(np.float32) / len(gt_match)

    # Pad with start and end values to simplify the math
    precisions = np.concatenate([[0], precisions, [0]])
    recalls = np.concatenate([[0], recalls, [1]])

    # Ensure precision values decrease but don't increase. This way, the
    # precision value at each recall threshold is the maximum it can be
    # for all following recall thresholds, as specified by the VOC paper.
    for i in range(len(precisions) - 2, -1, -1):
        precisions[i] = np.maximum(precisions[i], precisions[i + 1])

    # Compute mean AP over recall range
    indices = np.where(recalls[:-1] != recalls[1:])[0] + 1
    mAP = np.sum((recalls[indices] - recalls[indices - 1]) *
                 precisions[indices])

    return mAP, precisions, recalls, overlaps


def compute_ap_2(gt_boxes, gt_class_ids,
                 pred_boxes, pred_class_ids, pred_scores,
                 iou_threshold=0.5):
    # Trim zero padding and sort predictions by score from high to low
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    pred_boxes = trim_zeros(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]

    # Compute IoU overlaps [pred_boxes, gt_boxes]
    overlaps = compute_overlaps(pred_boxes, gt_boxes)
    print("object detected :", len(pred_boxes))
    print("Ground Truth :", len(gt_boxes))
    print('predicted :', pred_class_ids)
    print('ground truth :', gt_class_ids)
    # Loop through ground truth boxes and find matching predictions
    true_positive = 0
    false_positive = 0
    false_negative = 0
    check_false = 0
    pred_match = np.zeros([pred_boxes.shape[0]])
    gt_match = np.zeros([gt_boxes.shape[0]])
    # print("overlaps :", overlaps)
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] == 1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            print('iou {}: {}'.format(i, iou))
            if iou < iou_threshold:
                false_positive += 1
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                true_positive += 1
                gt_match[j] = 1
                pred_match[i] = 1
                break
            else:
                check_false += 1
    print("match count", true_positive)
    print("mismatch", false_positive)
    # Compute precision and recall at each prediction box step
    # pred match = true positive
    # precisions = true_positive / len(pred_match)
    # recalls = true_positive / len(gt_match)
    print('pred match', pred_match)
    false_negative = len(gt_match) - len(pred_match)
    if false_negative < 0:
        false_negative = 0
    print("false_negative", false_negative)
    accuracy = true_positive / \
        (false_negative + true_positive + false_positive)
    precisions = true_positive / len(pred_match)
    test = sum([true_positive, false_negative])
    if test == 0:
        test = 1
    recalls = true_positive / test

    # # # Pad with start and end values to simplify the math
    # precisions = np.concatenate([[0], precisions, [0]])
    # recalls = np.concatenate([[0], recalls, [1]])
    # print(recalls)
    # # compute the precision envelope
    # for i in range(precisions.size - 1, 0, -1):
    #     precisions[i - 1] = np.maximum(precisions[i - 1], precisions[i])

    # # to calculate area under PR curve, look for points
    # # where X axis (recall) changes value
    # i = np.where(recalls[1:] != recalls[:-1])[0]

    # # and sum (\Delta recall) * prec
    # mAP = np.sum((recalls[i + 1] - recalls[i]) * precisions[i + 1])
    result = {'ground truth': len(gt_boxes),
              'object detection': len(pred_boxes),
              'true positive': true_positive,
              'false positive': false_positive,
              'false negative': false_negative}
    return accuracy, precisions, recalls, result


def compute_ap_3(gt_boxes, gt_class_ids,
                 pred_boxes, pred_class_ids, pred_scores,
                 iou_threshold=0.5):
    # Trim zero padding and sort predictions by score from high to low
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    pred_boxes = trim_zeros(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]

    # Compute IoU overlaps [pred_boxes, gt_boxes]
    overlaps = compute_overlaps(pred_boxes, gt_boxes)
    print("object detected :", len(pred_boxes))
    print("Ground Truth :", len(gt_boxes))
    print('predicted :', pred_class_ids)
    print('ground truth :', gt_class_ids)
    # Loop through ground truth boxes and find matching predictions

    check_false = 0
    pred_match = np.zeros([pred_boxes.shape[0]])
    gt_match = np.zeros([gt_boxes.shape[0]])
    # print("overlaps :", overlaps)
    class_list = []
    accuracys = []
    precisions = []
    recalls = []

    true_positives = []
    false_positives = []
    false_negatives = []
    # get all class in GT
    for value in gt_class_ids:
        if value not in class_list:
            class_list.append(value)

    # calculate accuracy and etc by class
    for value in class_list:
        check_class = 0
        true_positive = 0
        false_positive = 0
        false_negative = 0
        for i in range(len(gt_boxes)):
            if gt_class_ids[i] == value:
                check_class += 1
        for i in range(len(pred_boxes)):
                # Find best matching ground truth box
            sorted_ixs = np.argsort(overlaps[i])[::-1]

            if pred_class_ids[i] == value:
                for j in sorted_ixs:
                        # If ground truth box is already matched, go to next one
                    if gt_match[j] == 1:
                        continue
                    # If we reach IoU smaller than the threshold, end the loop
                    iou = overlaps[i, j]
                    # print('iou {}: {}'.format(i, iou))
                    if iou < iou_threshold:
                        false_positive += 1
                        break
                    # else if
                    # Do we have a match?
                    if (pred_class_ids[i] == gt_class_ids[j]):
                        true_positive += 1
                        gt_match[j] = 1
                        pred_match[i] = 1
                        break
        print(check_class)
        false_negative += check_class - true_positive - false_positive
        print(false_negative)
        if false_negative < 0:
            false_negative = 0
        print("match count", true_positive)
        print('false positive', false_positive)
        print('false negative', false_negative)
        accuracy = sum([true_positive]) / \
            (false_positive + false_negative + true_positive if true_positive >
             0 else 1)
        precision = true_positive / \
            sum([true_positive if true_positive > 0 else 1, false_positive])
        recall = true_positive / \
            sum([true_positive if true_positive > 0 else 1, false_negative])
        accuracys.append(accuracy)
        recalls.append(recall)
        precisions.append(precision)
        true_positives.append(true_positive)
        false_positives.append(false_positive)
        false_negatives.append(false_negative)

        # if len(gt_boxes) < len(pred_boxes):
    #     #     false_positive = len(pred_boxes) - true_positive
    # print("match count", true_positive)
    # print('false negative', false_positive)
    # print('true negative', false_negative)
    # print("mismatch", gt_match)
    # print("check false", confusion_matrix(gt_class_ids, pred_class_ids))
    # Compute precision and recall at each prediction box step
    # pred match = true positive
    # precisions = true_positive / len(pred_match)
    # recalls = true_positive / len(gt_match)
    # print('pred match', sum(
    #     [true_positive if true_positive > 0 else 1, false_positive]))
    # false_negative = len(
    # #     gt_match) - len(pred_match)
    # if false_negative < 0:
    #     false_negative = 0
    # accuracy = sum([true_positive+true_negative]) / \
    #     (false_positive + true_positive + false_negative + true_negative)
    # precisions = true_positive / \
    #     sum([true_positive if true_positive > 0 else 1, false_positive])
    # recalls = true_positive / \
    #     sum([true_positive if true_positive > 0 else 1, false_negative])
    accuracys = np.mean(np.array(accuracys))
    precisions = np.mean(np.array(precisions))
    recalls = np.mean(np.array(recalls))
    # # # Pad with start and end values to simplify the math
    # precisions = np.concatenate([[0], precisions, [0]])
    # recalls = np.concatenate([[0], recalls, [1]])
    # print(recalls)
    # # compute the precision envelope
    # for i in range(precisions.size - 1, 0, -1):
    #     precisions[i - 1] = np.maximum(precisions[i - 1], precisions[i])

    # # to calculate area under PR curve, look for points
    # # where X axis (recall) changes value
    # i = np.where(recalls[1:] != recalls[:-1])[0]

    # # and sum (\Delta recall) * prec
    # mAP = np.sum((recalls[i + 1] - recalls[i]) * precisions[i + 1])
    result = {'ground truth': len(gt_boxes),
              'object detection': len(pred_boxes),
              'true positive': sum(true_positives),
              'false positive': sum(false_positives),
              'false negative': sum(false_negatives)}
    return accuracys, precisions, recalls, result


def compute_overlaps(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].

    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes
    area1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1])
    area2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1])

    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))
    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]
        overlaps[:, i] = compute_iou(box2, boxes1, area2[i], area1)
    return overlaps
