import tensorflow as tf
import keras.backend as K
from .Utils import batch_pack_graph

############################################################
#                           Losses                         #
############################################################


def rpnClassLoss(rpn_match, rpn_class_logits):
    """RPN anchor classifier loss.

    rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_class_logits: [batch, anchors, 2]. RPN classifier logits for FG/BG.
    """
    # Squeeze last dim to simplify
    rpn_match = tf.squeeze(rpn_match, -1)
    # Get anchor classes. Convert the -1/+1 match to 0/1 values.
    anchor_class = K.cast(K.equal(rpn_match, 1), "int32")
    # Positive and Negative anchors contribute to the loss,
    # but neutral anchors (match value = 0) don't.
    indices = tf.where(K.not_equal(rpn_match, 0))
    # Pick rows that contribute to the loss and filter out the rest.
    rpn_class_logits = tf.gather_nd(rpn_class_logits, indices)
    anchor_class = tf.gather_nd(anchor_class, indices)
    # Crossentropy loss
    loss = K.sparse_categorical_crossentropy(target=anchor_class,
                                             output=rpn_class_logits,
                                             from_logits=True)
    loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
    return loss


def rpnBboxLoss(target_bbox, rpn_match, rpn_bbox):
    """
    Return the RPN bounding box loss graph.
    """
    print("losses yang ada ", rpn_match)
    # Positive anchors contribute to the loss, but negative and
    # neutral anchors (match value of 0 or -1) don't.
    rpn_match = K.squeeze(rpn_match, -1)
    indices = tf.where(K.equal(rpn_match, 1))

    # Pick bbox deltas that contribute to the loss
    rpn_bbox = tf.gather_nd(rpn_bbox, indices)

    # Trim target bounding box deltas to the same length as rpn_bbox.
    batch_counts = K.sum(K.cast(K.equal(rpn_match, 1), "int32"), axis=1)
    target_bbox = batch_pack_graph(target_bbox, batch_counts,
                                   1)

    # TODO: use smooth_l1_loss() rather than reimplementing here
    #       to reduce code duplication
    diff = K.abs(target_bbox - rpn_bbox)
    less_than_one = K.cast(K.less(diff, 1.0), "float32")
    loss = (less_than_one * 0.5 * diff**2) + (1 - less_than_one) * (diff - 0.5)

    loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
    return loss


def resnetClassLoss(targetClassIds, predClass, activeClassIds):
    targetClassIds = tf.cast(targetClassIds, 'int64')

    # Find predictions of classes that are not in the dataset.
    predClassIds = tf.argmax(predClass, axis=2)
    predActive = tf.gather(activeClassIds[0], predClassIds)

    # Loss
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=targetClassIds, logits=predClass)

    # Erase losses of predictions from classes that are not defined
    loss = loss * predActive

    # Computer loss mean. Use only predictions that contribute
    # to the loss to get a correct mean.
    loss = tf.reduce_sum(loss) / tf.reduce_sum(predActive)
    return loss


def resnetBboxLoss(targetBbox, targetClassIds, predBbox):
        # Reshape to merge batch and roi dimensions for simplicity.
    targetClassIds = K.reshape(targetClassIds, (-1,))
    targetBbox = K.reshape(targetBbox, (-1, 4))
    predBbox = K.reshape(predBbox, (-1, K.int_shape(predBbox)[2], 4))

    # filter Roi until remain roi with positive object
    positiveRoi = tf.where(targetClassIds > 0)[:, 0]
    positiveRoiClassIds = tf.cast(
        tf.gather(targetClassIds, positiveRoi), tf.int64)
    indices = tf.stack([positiveRoi, positiveRoiClassIds], axis=1)

    targetBbox = tf.gather(targetBbox, positiveRoi)
    predBbox = tf.gather_nd(predBbox, indices)

    loss = K.switch(tf.size(targetBbox) > 0,
                    smooth_l1_loss(y_true=targetBbox, y_pred=predBbox),
                    tf.constant(0.0))
    loss = K.mean(loss)
    loss = K.reshape(loss, [1, 1])
    return loss

###################### custom losses ########################


def smooth_l1_loss(y_true, y_pred):
    diff = K.abs(y_true - y_pred)
    less_than_one = K.cast(K.less(diff, 1.0), "float32")
    loss = (less_than_one * 0.5 * diff**2) + (1 - less_than_one) * (diff - 0.5)
    return loss
