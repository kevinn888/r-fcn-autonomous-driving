import matplotlib.patches as patches
import matplotlib.pyplot as plt
import argparse
import numpy as np
import pickle
import time
from keras.preprocessing import image
import os
from Utils.Config import Config


class RFCNNConfig(Config):
    """Configuration for training on the toy shapes dataset.
    Derives from the base Config class and overrides values specific
    to the toy shapes dataset.
    """
    # Give the configuration a recognizable name
    NAME = "BDD"

    # Backbone model
    # choose one from ['resnet50', 'resnet101', 'resnet50_dilated', 'resnet101_dilated']
    BACKBONE = "resnet50"

    # Train on 1 GPU and 8 images per GPU. We can put multiple images on each
    # GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

    # Number of classes (including background)
    C = 1 + 4  # background + 2 tags
    NUM_CLASSES = C
    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 800
    IMAGE_MAX_DIM = 1280

    # Use smaller anchors because our image and objects are small
    RPN_ANCHOR_SCALES = (32, 64, 128, 256, 512)  # anchor side in pixels
    # Use same strides on stage 4-6 if use dilated resnet of DetNet
    # Like BACKBONE_STRIDES = [4, 8, 16, 16, 16]
    BACKBONE_STRIDES = [4, 8, 16, 32, 64]
    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    TRAIN_ROIS_PER_IMAGE = 200

    # Use a small epoch since the data is simple
    STEPS_PER_EPOCH = 100

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 5

    RPN_NMS_THRESHOLD = 0.7

    DETECTION_MIN_CONFIDENCE = 0.4
    POOL_SIZE = 7


def Test(model, loadpath, savepath):
    assert not loadpath == savepath, "loadpath should'n same with savepath"

    model_path = model.find_last()[1]
    # Load trained weights (fill in path to trained weights here)
    # print('INI MODEL PATHNYA', model_path)
    model.load_weights(model_path, by_name=True)
    # model.load_weights(
    #     "D:/Keras-RFCN_bdd_0147.h5", by_name=True)
    print("Loading weights from ", model_path)
    print(loadpath)
    if os.path.isdir(loadpath):
        for idx, imgname in enumerate(os.listdir(loadpath)):
            if not imgname.lower().endswith(('.bmp', '.jpeg', '.jpg', '.png', '.tif', '.tiff')):
                continue
            print(imgname)
            imageoriChannel = np.array(plt.imread(
                os.path.join(loadpath, imgname))) / 255.0
            img = image.img_to_array(image.load_img(
                os.path.join(loadpath, imgname)))
            TestSinglePic(img, imageoriChannel, model,
                          savepath=savepath, imgname=imgname)

    elif os.path.isfile(loadpath):
        if not loadpath.lower().endswith(('.bmp', '.jpeg', '.jpg', '.png', '.tif', '.tiff')):
            print("not image file!")
            return
        print(loadpath)
        imageoriChannel = np.array(plt.imread(loadpath)) / 255.0
        img = image.img_to_array(image.load_img(loadpath))
        (filename, extension) = os.path.splitext(loadpath)
        print(savepath)
        time = TestSinglePic(img, imageoriChannel, model,
                             savepath=savepath, imgname=filename)
        return time


def Test_sc(model, loadpath, savepath):
    assert not loadpath == savepath, "loadpath should'n same with savepath"

    model_path = model.find_last()[1]
    # Load trained weights (fill in path to trained weights here)
    # print('INI MODEL PATHNYA', model_path)
    model.load_weights(
        "D:\Workspace\College\Semester 8\Tugas Akhir\codeR-FCN\model-data\Keras-RFCN_bdd_0293.h5", by_name=True)
    # model.load_weights(
    #     "D:/Keras-RFCN_bdd_0147.h5", by_name=True)
    print("Loading weights from ", model_path)
    print(loadpath)
    if os.path.isdir(loadpath):
        for idx, imgname in enumerate(os.listdir(loadpath)):
            if not imgname.lower().endswith(('.bmp', '.jpeg', '.jpg', '.png', '.tif', '.tiff')):
                continue
            print(imgname)
            imageoriChannel = np.array(plt.imread(
                os.path.join(loadpath, imgname))) / 255.0
            img = image.img_to_array(image.load_img(
                os.path.join(loadpath, imgname)))
            TestSinglePic(img, imageoriChannel, model,
                          savepath=savepath, imgname=imgname)

    elif os.path.isfile(loadpath):
        if not loadpath.lower().endswith(('.bmp', '.jpeg', '.jpg', '.png', '.tif', '.tiff')):
            print("not image file!")
            return
        print(loadpath)
        imageoriChannel = np.array(plt.imread(loadpath)) / 255.0
        img = image.img_to_array(image.load_img(loadpath))
        (filename, extension) = os.path.splitext(loadpath)
        print(savepath)
        time = TestSinglePic(img, imageoriChannel, model,
                             savepath=savepath, imgname=filename)
        return time


def TestSinglePic(image, image_ori, model, savepath, imgname):
    import time
    start_time = time.time()
    r = model.detect([image], verbose=1)[0]
    processed_time = time.time() - start_time
    print("--- %s seconds ---" % (processed_time))
    print(r)

    def get_ax(rows=1, cols=1, size=8):
        _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
        return ax

    ax = get_ax(1)

    assert not savepath == "", "empty save path"
    assert not imgname == "", "empty image file name"

    for box in r['rois']:
        y1, x1, y2, x2 = box
        p = patches.Rectangle((x1, y1), x2 - x1, y2 - y1, linewidth=2,
                              alpha=0.7, edgecolor="red", facecolor='none')
        ax.add_patch(p)
    ax.imshow(image_ori)
    plt.savefig(os.path.join(savepath, imgname), bbox_inches='tight')
    plt.show()
    plt.clf()
    return time
