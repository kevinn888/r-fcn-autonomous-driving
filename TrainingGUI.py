import os
import logging
import tkinter as tk
from tkinter import filedialog


def askDirectory():
    global folder_path
    global b
    directory = filedialog.askdirectory()
    folder_path.set(directory)
    b['state'] = "normal"


def runCommand(text):
    print(text)
    label_path = os.path.join(text.get(), 'labels')
    images_path = os.path.join(text.get(), 'images/100k')
    Path_args = os.path.join(
        os.getcwd(), 'training.py" --images="{}" --label="{}"'.format(images_path, label_path))
    os.path.normpath
    cmd = 'CMD /C C:/Users/kevin/anaconda3/envs/tf-gpu/python.exe "' + Path_args
    print(cmd)
    os.system(cmd)


if __name__ == "__main__":
    print(os.getcwd())

    m = tk.Tk(screenName="RFCN Training")
    m.title("RFCN Training")
    folder_path = tk.StringVar()
    b = tk.Button(m, text='Training', width=25,
                  command=lambda x=folder_path: runCommand(x), bg="#FF5733")
    b['state'] = 'disabled'
    openFile = tk.Button(m, text='open Dataset Image Directory',
                         width=25, command=askDirectory)
    openFile.grid(row=0, column=1, padx=10, pady=10)
    tk.Label(m, text="dataset directory :").grid(column=1)
    tk.Label(m, textvariable=folder_path).grid(column=1, padx=30, pady=10)
    tk.Label(
        m, text="Model will be save in directory './model-data'").grid(column=1, padx=30)
    b.grid(column=1)
    tk.Button(m, text='Exit', width=25,
              command=m.destroy).grid(column=1, padx=10, pady=10)
    # e1 = Entry(master, option=value)
    m.mainloop()
